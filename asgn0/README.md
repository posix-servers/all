Written by David Wu and Rory Landau
For CSE 130, Faisal Nawab, UCSC, Fall 2020

# Overview
Dog is a cat (minus flags) alternative.
Its architecture is described in DESIGN.pdf.

# Compiling and Running
Dog can and should be compiled using the `make` command.
It can then be run via `./dog <file1> <file2> ... <fileN>`


# Capabilities and Limitations

# Style Guide

* Indentation is 4 spaces

* Variable declaration, other than explicitly temporary variables, should be performed at the start of the variable's subroutine.

* Lines should not exceed 80 characters.
