#include <fcntl.h> // used for open()
#include <unistd.h> // used for read(), write(), close()
#include <stdio.h>
#include <errno.h> // used for perror()
#include <string.h>

//CONSTANTS
#define MAX_BUF_SIZE 32768 // 32 KiB

/*------------------------------------------------------------------------------
Notes
------------------------------------------------------------------------------*/
// 0 is Standard Input
// 1 is Standard Output
// 2 is Standard Error

/*------------------------------------------------------------------------------
Header Functions
------------------------------------------------------------------------------*/
int read_from_stdin(char* buffer);

/*------------------------------------------------------------------------------
MAIN
------------------------------------------------------------------------------*/
int main (int argc, char *argv[]) {

    // variables
    int     fd;                         // file descriptor
    char    buf[MAX_BUF_SIZE];          // buffer, set to constant

   // echo stdin in a loop if no arguments given, end if EOF sig sent
   while(argc == 1 && read_from_stdin(buf));

   // go through all of user's inputs
   for (int i = 1; i < argc; ++i) {
       memset(buf, 0, sizeof(buf)); // Init buffer

       // Echo stdin if - is entered, stopping when EOF sig sent
       while (!strcmp(argv[i], "-") && read_from_stdin(buf));

       fd = open(argv[i], O_RDONLY); // try to open user's file
       if(fd != -1) { // check for file errors
           static int ret; // using static to save mem space
           // while not EOF, read file, capture res
           while( (ret = read(fd, buf, sizeof(buf))) )
               write(1, buf, ret); // write buffer to stdout, up to read res
       } else fprintf(stderr, "dog: %s: %s\n", argv[i], strerror(errno));

   }
   close(fd); // close the file

   return 0;
}

/*------------------------------------------------------------------------------
SUBROUTINES
------------------------------------------------------------------------------*/
/* read_from_stdin
@params - buffer: the fixed-size buffer to read into
@returns number of bytes read from stdin, where 0 is EOF */
int read_from_stdin(char* buffer){
    int read_bytes;

    // compilier complains if I don't use constant. Functionally the same though
    memset(buffer, 0, MAX_BUF_SIZE);
    read_bytes = read(0, buffer, sizeof(buffer));
    write(1, buffer, sizeof(buffer));

    return read_bytes;
}
