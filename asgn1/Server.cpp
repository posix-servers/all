#include "Server.hpp"

/* Server.hpp
Implements the class "Server". */

/*------------------------------------------------------------------------------
Constructors
------------------------------------------------------------------------------*/

//IP and Port
Server::Server(const char* ip, const char* port){
    memset(buffer, 0, sizeof(buffer));
    memset(file_name, 0, sizeof(file_name));
    content_length = 0;

    create_socket(ip, port);
    run_server();
}


// Copy Constructor
Server::Server(const Server &source){
    copy(source);

    return;
}

/*------------------------------------------------------------------------------
Subroutines
------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
Destructor
------------------------------------------------------------------------------*/
/* In the current implementation, the dest is unlikely to ever be called as the
prog will be force-killed */
Server::~Server(){}

/*------------------------------------------------------------------------------
Operators
------------------------------------------------------------------------------*/
// Assignment Operator
Server& Server::operator=(const Server &source){
    copy(source);

    return *this;
}

/*------------------------------------------------------------------------------
Private Functions
------------------------------------------------------------------------------*/
void Server::copy(const Server &source){
    server_fd = source.server_fd;
    server_addr = source.server_addr;

    client_fd = source.client_fd;
    client_addr = source.client_addr;
    client_addr_len = source.client_addr_len;

    strcpy(buffer, source.buffer);

    request_type = source.request_type;
    strcpy(file_name, source.file_name);
    content_length = source.content_length;

    return;
}

/*------------------------------------------------------------------------------
Server Setup
------------------------------------------------------------------------------*/

/* get_addr 
Uses getaddrinfo or inet_aton to translate the given IP or hostname into a
bind-usable form.
@params - char* name: the domain/IP to translate 
@returns translated, numeric IP respresentation */
unsigned long Server::get_addr(const char* name_in) {
    unsigned long toReturn;
    struct addrinfo hints, *info;
    char* name = (char*) name_in;

    // reserve and 0-fill
    memset(&hints, 0, sizeof(hints));

    // declare options 
    hints.ai_family = AF_INET;

    // peel off "http://"
    if(strstr(name, "https://")) name += 8;
    if(strstr(name, "http://")) name += 7;

    // resolve name
    if (getaddrinfo(name, NULL, &hints, &info) == -1) errno_kill("GETADDRINFO", 1);

    // retrieve translated addr
    toReturn = ((struct sockaddr_in*) info->ai_addr)->sin_addr.s_addr;
    freeaddrinfo(info);
    return toReturn;
}


// Creates and initializes socket.
void Server::create_socket(const char* ip, const char* port){
    // Create main listening socket
    server_fd = socket(AF_INET, SOCK_STREAM, 0); //IP, TCP, {unused}
    if(server_fd == -1) errno_kill("SOCKET", 1);

    // Construct sockaddr
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET; //IP protocol
    server_addr.sin_addr.s_addr = get_addr(ip);
    //    and convert port to unsigned short
    server_addr.sin_port = htons((unsigned short) strtoul(port, NULL, 0));

    // bind, using the result of getaddrinfo
    if(bind(server_fd,(struct sockaddr*)&server_addr,sizeof(server_addr)) == -1)
        errno_kill("PORT_ALREADY_IN_USE", 1);
    return;
}


/* run_server
Main operation loop of the server, once up and running.
Continuously listens for requests from clients and executes them.
Heavy lifting performed via service_client.
@params -
@returns void */
void Server::run_server(){

    if (listen(server_fd, BACK_LOG) == -1) errno_kill("CLOSE_LISTEN", 1);
    // while server is up provide service to any potential client
    int serv_res = 1;
    while(serv_res) serv_res = service_client();

    if (close(server_fd) == -1) errno_kill("CLOSE_SERVER", 1);

    return;
}


/*------------------------------------------------------------------------------
service_client
------------------------------------------------------------------------------*/
/* service_client
Accepts and intermediaries between server and client using {helper function}.
Only ever returns true, as it has no kill condition. If the server should be
client-killable, allow a return of 0.
@params -
@returns true */
int Server::service_client(){

    // connect to client
    client_fd = accept(server_fd, &client_addr, &client_addr_len);
    if(client_fd == -1){
        errno_kill("CLOSE_ACCEPT", 0);
        if (close(client_fd) == -1) errno_kill("CLOSE_CLIENT", 1);

        return 1;
    }

    // store client's request
    to_buffer(client_fd); // store raw input into buffer
    parse_request(); // parse the buffer

    // check validity of file name
    if(!test_name(file_name)){
        send_response(client_fd, HTTP_BAD_REQUEST); // invalid file name
        if (close(client_fd) == -1) errno_kill("CLOSE_CLIENT", 1);

        return 1;
    }

    // honor the request
    switch(request_type){
        case GET: // retrieve and return a file
        { 
            do_get_request();
            break;
        }

        case PUT: // write request body to file
        { 
            do_put_request();
            break;
        }

        default: 
        {
            send_response(client_fd, HTTP_BAD_REQUEST); 

            break;
        }
    }

    if (close(client_fd) == -1) errno_kill("CLOSE_CLIENT", 1);

    return 1;
}


/*------------------------------------------------------------------------------
Request Handling
------------------------------------------------------------------------------*/
/* to_buffer
Reads file descriptor and writes into Server's buffer.
@params int fd: file discriptor of the contents to be buffered
@returns ssize_t: length of bytes */
ssize_t Server::to_buffer(const int fd){

    ssize_t len = 0;

    memset(buffer, 0, sizeof(buffer)); // flush the buffer
    len = recv(fd, buffer, sizeof(buffer), 0);
    if (len == -1) errno_kill("CLOSE_REQUEST", 1);

    return len;
}

/* parse_request
Analyzes a copy of buffer and saves type of request and resource name into
Server's variables.
@params
@returns void */
 void Server::parse_request(){

    memset(file_name, 0, sizeof(file_name));
    request_type = EMPTY;
    content_length = 0;

    char buffer_copy[BUFFER_SIZE];
    char* token;

    strcpy(buffer_copy, buffer); // copy; tokenizing is destructive.

    // GET REQUEST TYPE
    token = strtok(buffer_copy, DELIMITERS);

    if(strstr(token, "GET"))
        request_type = GET;
    else if(strstr(token, "PUT"))
        request_type = PUT;

    // GET FILE NAME
    token = strtok(NULL, DELIMITERS);
    token++; // gets rid of '/'

    strcpy(file_name, token);

    // GET CONTENT LENGTH
    if(strstr(buffer, "Content-Length:")){ // if buffer has content length

        // discard tokens until you find content length
        while ( !strstr( (token = strtok(NULL, DELIMITERS)), "Content-Length:" ) );

        token = strtok(NULL, DELIMITERS); // get the number
        content_length = strtol(token, NULL, 10); // save the number
    } 

    return;
 }


/* do_get_request
Helper function to stream a file to the client
@params
@returns void */
void Server::do_get_request(){
    int fd = 0;
    ssize_t byte_count = 0;
    ssize_t content_len = 0;

    memset(buffer, 0, sizeof(buffer)); // init buffer

    // open file
    fd = open(file_name, O_RDONLY);
    if(fd == -1){
        // permission error
        if(errno == EACCES)        send_response(client_fd, HTTP_FORBIDDEN);
        // not found
        else if (errno == ENOENT)   send_response(client_fd, HTTP_NOT_FOUND);
        // other
        else                        send_response(client_fd, HTTP_INTERNAL_SERVER_ERROR);
        close(fd);
        return;
    }

    // get content length of file
    if ( (content_len = (ssize_t) lseek(fd, 0, SEEK_END)) == -1 ){  // lseek returns off_t
        close(fd);
        return;     
    }
    if (lseek(fd, 0, SEEK_SET) == -1){ // rewind file back to beginning
        close(fd);
        return;
    }

    // send http header to client with content length
    send_response(client_fd, HTTP_OK, content_len);

    // while not EOF, read and send chunks of file
    byte_count = read(fd, buffer, sizeof(buffer));
    while ( byte_count > 0 ){

        if(send(client_fd, buffer, byte_count, 0) == -1) {
            close(fd);
            errno_kill("BODY_SEND", 0);
            return;
        }
        
        byte_count = read(fd, buffer, sizeof(buffer));
    }

    close(fd);

    return;
 }


/* do_put_request
Performs all the operations required for handling a PUT request, including:
Opening the file to write, returning a 100-continue, checking content-length,
reading until EOF or length, streaming read data into file, and returning the
relevant HTTP status code.
@params - 
@returns void */
void Server::do_put_request(){

    ssize_t buf_len = 0;
    int fd = open(file_name, O_WRONLY | O_CREAT | O_TRUNC, 0644);

    if(fd == -1){
        switch (errno){
            case EACCES:    send_response(client_fd, HTTP_FORBIDDEN); break; // permission error
            case ENOENT:    send_response(client_fd, HTTP_NOT_FOUND); break; // not found
            default:        send_response(client_fd, HTTP_INTERNAL_SERVER_ERROR); break;
        }

        close(fd);
        return;
    }

    send_response(client_fd, HTTP_CONTINUE); // tell client to start sending data
    // if client provided content length
    if(content_length >= 0){
        long byte_count = 0;

        // read content length amount of bytes or until client dies
        while(byte_count < content_length){
            memset(buffer, 0, sizeof(buffer));
            buf_len = recv(client_fd, buffer, sizeof(buffer), 0);
            if (buf_len <= 0){ // if recv() gives -1(error) or a 0(EOF)
                close(fd);
                return;
            }

            byte_count += buf_len;

            if (write(fd, buffer, buf_len) <= 0){ // if write() gives -1(error) or a 0(EOF)
                close(fd);
                return;
            }
        }
        
        // success!!
        close(fd);
        send_response(client_fd, HTTP_CREATED); 
        return;
    }

    // client never said when to stop, so keep going until they die or EOF is sent.
    buf_len = recv(client_fd, buffer, sizeof(buffer), 0); // Hangs here when given empty file
    while( buf_len > 0){ // buf_len != -1 && buf_len > 0
        if (write(fd, buffer, buf_len) <= 0){ // if write() gives -1(error) or a 0(EOF)
            close(fd);
            return;
        }

        buf_len = recv(client_fd, buffer, sizeof(buffer), 0);
    }

    send_response(client_fd, HTTP_CREATED); 

    close(fd);
    return;
}


/*------------------------------------------------------------------------------
File Operations 
------------------------------------------------------------------------------*/
/* test_name
Tests a given string for character compliance.
Checks length and attempts to match a string via REGEX.
Matches against any combination of upper/lowercase letters or digits.
I miss Perl's regex splitting. --Rory
@params - str: the testee
@returns true or false */
int Server::test_name(char* str){
    return (strlen(str) == FILE_NAME_SIZE && regex_match(str, regex(REGEX)));
}


/*------------------------------------------------------------------------------
Response Creation
------------------------------------------------------------------------------*/
/* send_response
Sends the HTTP header only as response to client. Doesn't send file data.
OK, Created, Bad Request, Forbidden, Not Found, Internal Server Error
@params - int: The socket to send the response to. Usually the client.
        - char*: The message to send. ie HTTP_OK
        - ssize_t: The length of the content.
@returns void */
void Server::send_response(int socket, const char* response, ssize_t content_len){

    if (socket <= 0) errno_kill("NO_SOCKET_RESPONSE", 1);

    char header[RESPONSE_SIZE];
    memset(header, 0, sizeof(header));

    // "HTTP\1.1 "
    strcat(header, VERSION_HTTP);
    strcat(header, " ");

    // "HTTP\1.1 200 OK\r\n"
    strcat(header, response);
    strcat(header, "\r\n");

    // "HTTP\1.1 200 OK\r\nContent-Length:0"
    strcat(header, "Content-Length: ");
    strcat(header, to_string(content_len).c_str());
    strcat(header, "\r\n\r\n");

    // send completed msg; error check
    if(send(socket, header, strlen(header), 0) == -1) errno_kill("HEADER_SEND", 0);

    return;
}


/*------------------------------------------------------------------------------
OTHER
------------------------------------------------------------------------------*/
/* errno_kill
A super duper simple function so I can stop writing more or less the same two
lines every time we error check. --Rory
The string would be a char* but C++ freaks out. Dunno why.
@params -   string id: an identifier for code location
            boolean kill: should this kill?
@returns void */
void errno_kill(const char* id, bool kill){
    fprintf(stderr, "ERROR_%s: %d\n%s\n", id, errno, strerror(errno));
    if(kill) exit(EXIT_FAILURE);
}
