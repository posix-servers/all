#include "Server.hpp"

using namespace std;

/*------------------------------------------------------------------------------
Notes
------------------------------------------------------------------------------*/
// 0 is Standard Input
// 1 is Standard Output
// 2 is Standard Error


/*------------------------------------------------------------------------------
MAIN
------------------------------------------------------------------------------*/
int main (int argc, char** argv) {

    // check for no args
    if(argc == 1){
        fprintf(stdout, "Syntax: httpserver <domain> <port>\n");
        exit(0);
    }

    // initalize server class
    //  we ignore all args beyond [1] and [2]
    Server serv = (argc == 2) ? Server(argv[1]) : Server(argv[1], argv[2]);
}
