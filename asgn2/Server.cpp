#include "Server.hpp"

using std::vector;
using std::regex;
using std::to_string;

/* Server.cpp
Implements the class "Server". */

/*------------------------------------------------------------------------------
Constructors
------------------------------------------------------------------------------*/

//IP and Port
Server::Server(const char* ip,
               const char* port,
               const int new_thread_count,
               const bool new_is_redundant){

    // Init redundancy
    is_redundant = new_is_redundant;

    // Init locker
    locker = new Lock_Manager(is_redundant);

    // Init threads
    total_thread_count = new_thread_count;
    init_workers(total_thread_count);

    // Init socket
    create_socket(ip, port);

    // Start the dispatcher
    dispatch();
}


// Copy Constructor
Server::Server(const Server &source){
    copy(source);

    return;
}

/*------------------------------------------------------------------------------
Destructor
------------------------------------------------------------------------------*/
/* In the current implementation, the dest is unlikely to ever be called as the
prog will be force-killed */
Server::~Server(){
    delete locker;
    sleep(SLEEP_TIME);
    for (auto it = workers.begin(); it != workers.end(); ++it) delete *it;
    workers.clear();
}

/*------------------------------------------------------------------------------
Operators
------------------------------------------------------------------------------*/
// Assignment Operator
Server& Server::operator=(const Server &source){
    copy(source);

    return *this;
}

/*------------------------------------------------------------------------------
Private Functions
------------------------------------------------------------------------------*/
void Server::copy(const Server &source){
    server_fd = source.server_fd;
    server_addr = source.server_addr;

    client_fd = source.client_fd;
    client_addr = source.client_addr;
    client_addr_len = source.client_addr_len;

    //total_thread_count = source.total_thread_count;
    is_redundant = source.is_redundant;

    return;
}

// Server Setup-----------------------------------------------------------------

/* get_addr 
Uses getaddrinfo or inet_aton to translate the given IP or hostname into a
bind-usable form.
@params - char* name: the domain/IP to translate 
@returns translated, numeric IP respresentation */
unsigned long Server::get_addr(const char* name_in) {
    unsigned long toReturn;
    struct addrinfo hints, *info;
    char* name = (char*) name_in;

    // reserve and 0-fill
    memset(&hints, 0, sizeof(hints));

    // declare options 
    hints.ai_family = AF_INET;

    // peel off "http://"
    if(strstr(name, "https://")) name += 8;
    if(strstr(name, "http://")) name += 7;

    // resolve name
    if (getaddrinfo(name, NULL, &hints, &info) == -1) errno_kill("GETADDRINFO", 1);

    // retrieve translated addr
    toReturn = ((struct sockaddr_in*) info->ai_addr)->sin_addr.s_addr;
    freeaddrinfo(info);
    return toReturn;
}

/* init_threads
Helper function used by constructor to initialize worker threads.
@params -int: amount of worker threads to start
@returns void */
void Server::init_workers(int worker_count){
    // init consumers
    for(int i = 0; i < worker_count; ++i){
        Worker* new_worker = new Worker(locker, i);
        workers.push_back(new_worker);
    }

    return;
}

// Creates and initializes socket.
void Server::create_socket(const char* ip, const char* port){
    // Create main listening socket
    server_fd = socket(AF_INET, SOCK_STREAM, 0); //IP, TCP, {unused}
    if(server_fd == -1) errno_kill("SOCKET", 1);

    // Construct sockaddr
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET; //IP protocol
    server_addr.sin_addr.s_addr = get_addr(ip);
    //    and convert port to unsigned short
    server_addr.sin_port = htons((unsigned short) strtoul(port, NULL, 0));

    // bind, using the result of getaddrinfo
    if(bind(server_fd,(struct sockaddr*)&server_addr,sizeof(server_addr)) == -1)
        errno_kill("PORT_ALREADY_IN_USE", 1);
    return;
}


// MAIN DISPATCH LOOP-----------------------------------------------------------

/* dispatch
Runs the dispatcher thread in a loop, feeding clients to locker's queue.
So long as the server is up and running, this sub does not end.
@params -
@returns void */
void Server::dispatch(){
    int run = 1;
    if (listen(server_fd, BACK_LOG) == -1) errno_kill("CLOSE_LISTEN", 1);
    
    // continuously feed clients to queue as they become available
    while(run){
        // accept a client and test it for errors
        // if it fails, close it
        // if it passes, queue it
        client_addr_len = sizeof(struct sockaddr);
        int c_fd = accept(server_fd, &client_addr, &client_addr_len);
        if(c_fd == -1){ //test the client
            errno_kill("CLOSE_ACCEPT", 0);
            if (close(client_fd) == -1) errno_kill("CLOSE_CLIENT", 1);
        } else locker->queue_client(c_fd);
    }

    if (close(server_fd) == -1) errno_kill("CLOSE_SERVER", 1);

    return;
}
