#include "Worker.hpp"

using std::to_string;
using std::regex;
using std::string;

/* Worker.cpp
Implements the class "Worker". */

/* start_working
Start up routine of worker thread that runs upon creation.
BIG NOTE:This function is one big workaround for giving a NONstatic member function to
    pthread_create() and is not technically part of Worker. And as such has some downsides:
    1. start_working() must be declared before using pthread_create(). Hence why it is up here.
    2. To reference member variables/functions of this Worker, you must use me->SomeVarOrFunc
@params -void* a reference to the data to be worked on.
@returns void* unused */
void* start_working(void* data){

    Worker* me = (Worker*) data;
    me->execute();
    
    return NULL;
}

/*------------------------------------------------------------------------------
Constructors
------------------------------------------------------------------------------*/
Worker::Worker(Lock_Manager *l, int new_id){

    header_len = 0;

    // NOTE: buffer, file_name, and C-L are reset to zero in execute()
    is_working = false;
    shared_locker = l;
    id = new_id;
    pthread_create(&thread, NULL, &start_working, this);

    return;
}

Worker::Worker(const Worker &source){
    copy(source);
    return;
}

/*------------------------------------------------------------------------------
Destructor
------------------------------------------------------------------------------*/
Worker::~Worker(){
    pthread_join(thread, NULL);
}

/*------------------------------------------------------------------------------
Operators
------------------------------------------------------------------------------*/
// Assignment operator
Worker& Worker::operator=(const Worker &source){
    copy(source);

    return *this;
}


/*------------------------------------------------------------------------------
Public Functions
------------------------------------------------------------------------------*/
/* get_thread
Returns a reference to Worker's thread
@params
@returns pthread_t*  */
pthread_t* Worker::get_thread() {return &thread;}


/* get_buffer
Returns a reference to Worker's buffer
@params
@returns char*  */
char* Worker::get_buffer() {return buffer;}


/* get_is_working
Returns a reference to Worker's is_working
@params
@returns char*  */
bool* Worker::get_is_working() {return &is_working;}

/*------------------------------------------------------------------------------
Private Functions
------------------------------------------------------------------------------*/
/* execute
The main function a thread operates on.
execute() is wrapped by the static function start_working().
@params - 
@returns (irrelevant) */
void Worker::execute(){

    // this is the loop every worker operates on over its lifetime.
    // dequeue_client() will automatically sleep it if no clients are queued.
    while(1){
        // empty info, prepare for the client
        memset(buffer, 0, sizeof(buffer));
        file_name.clear();
        content_length = 0;
        // get a fd from the queue
        client_fd = shared_locker->dequeue_client();
        // now that we have a client, operate on it
        service_client();
    }

    return;
}

/* service_client
Intermediaries between this thread and dequeued client.
@params -
@returns void */
void Worker::service_client(){
    // store client's request
    to_buffer(client_fd); // store raw input into buffer


    char header[RESPONSE_SIZE];
    header_len = strlen(buffer) - strlen(strstr(buffer, "\r\n\r\n") + 4); // +4 deletes \r\n\r\n

    memcpy( header, &buffer, header_len);

    parse_request(header); // parse the buffer

    // check validity of file name
    if(!test_name(file_name)){
        send_response(client_fd, HTTP_BAD_REQUEST); // invalid file name
        if (close(client_fd) == -1) errno_kill("CLOSE_CLIENT", 1);

        return;
    }

    // honor the request
    switch(request){
        case GET: // retrieve and return a file
        { 
            do_get_request();
            break;
        }

        case PUT: // write request body to file
        { 
            do_put_request();
            break;
        }

        default: 
        {
            send_response(client_fd, HTTP_INTERNAL_SERVER_ERROR); 
            break;
        }
    }

    if (close(client_fd) == -1) errno_kill("CLOSE_CLIENT", 1);
    return;
}

// Request Handling-------------------------------------------------------------

/* to_buffer
Reads file descriptor and writes into Worker's buffer.
@params int fd: file discriptor of the contents to be buffered
@returns ssize_t: length of bytes */
ssize_t Worker::to_buffer(const int fd){

    ssize_t len = 0;

    memset(buffer, 0, sizeof(buffer)); // flush the buffer
    len = recv(fd, buffer, sizeof(buffer), 0);
    if (len == -1) errno_kill("CLOSE_REQUEST", 1);

    return len;
}

/* parse_request
Analyzes header and saves type of request and resource name into
Worker's variables.
Note: This function destroys the header.
@params char*: header string
@returns void */
void Worker::parse_request(char* header){

    bool has_content_length = strstr(header, "Content-Length:");
    char* save_pointer;

    // init member variables
    file_name.clear();
    request = EMPTY;
    content_length = 0;

    char* token;

    // GET REQUEST TYPE
    token = strtok_r(header, DELIMITERS, &save_pointer); 
    if(strstr(token, "GET"))
        request = GET;
    else if(strstr(token, "PUT"))
        request = PUT;

    // GET FILE NAME
    token = strtok_r(NULL, DELIMITERS, &save_pointer);
    token++; // gets rid of '/'

    file_name += token;

    // GET CONTENT LENGTH
    if(has_content_length){ // if header has content length

        // discard tokens until you find content length
        while ( !strstr( (token = strtok_r(NULL, DELIMITERS, &save_pointer)), "Content-Length:" ) );

        token = strtok_r(NULL, DELIMITERS, &save_pointer); // get the number
        content_length = strtol(token, NULL, 10); // save the number
    } 

    return;
}

/* do_get_request
Helper function to stream a file to the client
@params
@returns void */
void Worker::do_get_request(){
    int fd = -1;
    ssize_t byte_count = 0;
    ssize_t content_len = 0;

    memset(buffer, 0, sizeof(buffer)); // init buffer

    // if the lock fails, the file is not in the hash
    if(shared_locker->lock_file(file_name) == 0) send_response(client_fd, HTTP_NOT_FOUND);
    else {
        // if redundancy is enable, call find_returnable to figure out fd to read from.
        //      find_returnable does NOT close the fd if it finds a valid one. 
        // if redundancy is disabled, just open the current directory's file
        if(shared_locker->redundancy()) {
            fd = find_returnable();
            switch(fd){
                case -1: // permission error
                {
                     send_response(client_fd, HTTP_FORBIDDEN); break;
                }
                case -2: // file not found
                {
                    send_response(client_fd, HTTP_NOT_FOUND); break;
                }
                case -3: // other
                {
                    send_response(client_fd, HTTP_INTERNAL_SERVER_ERROR); break;
                }
                // do nothing if non-error
            }
        }
        else{
            fd = open(file_name.c_str(), O_RDONLY);
            if(fd == -1){
                // permission error
                if(errno == EACCES)         send_response(client_fd, HTTP_FORBIDDEN);
                // not found
                else if (errno == ENOENT)   send_response(client_fd, HTTP_NOT_FOUND);
                // other
                else                        send_response(client_fd, HTTP_INTERNAL_SERVER_ERROR);
                close_file(fd);
                return;
            }
        }

        // get content length of file
        if ( (content_len = (ssize_t) lseek(fd, 0, SEEK_END)) == -1 ){  // lseek returns off_t
            close_file(fd);
            return;
        }
        if (lseek(fd, 0, SEEK_SET) == -1){ // rewind file back to beginning
            close_file(fd);
            return;
        }

        // send http header to client with content length
        send_response(client_fd, HTTP_OK, content_len);

        // while not EOF, read and send chunks of file
        byte_count = read(fd, buffer, sizeof(buffer));
        while ( byte_count > 0 ){

            if(send(client_fd, buffer, byte_count, 0) == -1) {
                close_file(fd);
                errno_kill("BODY_SEND", 0);
                return;
            }
            
            byte_count = read(fd, buffer, sizeof(buffer));
        }

        close_file(fd);
    }
    return;
}

/* do_put_request
Performs all the operations required for handling a PUT request, including:
Opening the file to write, returning a 100-continue, checking content-length,
reading until EOF or length, streaming read data into file, and returning the
relevant HTTP status code.
@params - 
@returns void */
void Worker::do_put_request(){
    ssize_t buf_len = 0;
    char* buffer_ptr = buffer + header_len; // remove possible header from buffer
    long byte_count = strlen(buffer_ptr); // 0 or size of leftover data
    header_len = 0; // don't need any more, init.

    // copy#/########## + 1 for terminator
    char fn[17], fn_2[17], fn_3[17]; // 2 and 3 are only used if -r
    int fd = -1, fd_2 = -1, fd_3 = -1; // as above, so below
    int r = shared_locker->redundancy();

    // if the file is not in the map already, add it and create its lock
    // add_lock does this check for us
    shared_locker->add_lock(file_name);
    // lock file
    shared_locker->lock_file(file_name);
    // open file 
    //  if -r, open files in copy[1...3]
    if(r){
        snprintf(fn, sizeof(fn), "copy1/%s", file_name.c_str());
        fd = open(fn, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        snprintf(fn_2, sizeof(fn_2), "copy2/%s", file_name.c_str());
        fd_2 = open(fn_2, O_WRONLY | O_CREAT | O_TRUNC, 0644);
        snprintf(fn_3, sizeof(fn_3), "copy3/%s", file_name.c_str());
        fd_3 = open(fn_3, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    } else { // if !-r, just open the given one
        fd = open(file_name.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644);
    }
    
    // test the main file
    if(fd == -1){
        switch (errno){
            case EACCES:    send_response(client_fd, HTTP_FORBIDDEN); break; // permission error
            case ENOENT:    send_response(client_fd, HTTP_NOT_FOUND); break; // not found
            default:        send_response(client_fd, HTTP_INTERNAL_SERVER_ERROR); break;
        }

        close_file(fd);
        if(r){ close(fd_2); close(fd_3); }
        return;
    }

    // Took header out. Write leftovers to file if there is any.
    if(strlen(buffer_ptr)) {
        write(fd, buffer_ptr, strlen(buffer_ptr));

        if(r){ 
            write(fd_2, buffer_ptr, strlen(buffer_ptr)); 
            write(fd_3, buffer_ptr, strlen(buffer_ptr));
        }

        memset(buffer, 0, sizeof(buffer));
    }

    send_response(client_fd, HTTP_CONTINUE); // tell client to start sending data
    // if client provided content length
    if(content_length > 0){

        // read content length amount of bytes or until client dies
        while(byte_count < content_length){
            memset(buffer, 0, sizeof(buffer));
            buf_len = recv(client_fd, buffer, sizeof(buffer), 0);
            //if (buf_len < 0) break; // if recv() gives -1(error) or a 0(EOF)

            byte_count += buf_len;
            
            // write buffer to file descriptor(s)
            int ret = write(fd, buffer, buf_len);
            if(r){ write(fd_2, buffer, buf_len); write(fd_3, buffer, buf_len); }
            if (ret <= 0) break; // if write() gives -1(error) or a 0(EOF)
        }
    } else {
        // client never said when to stop, so keep going until they die or EOF is sent.
        buf_len = recv(client_fd, buffer, sizeof(buffer), 0); // Hangs here when given empty file

        while( buf_len > 0 ){ // buf_len != -1 && buf_len > 0
            int ret = write(fd, buffer, buf_len);
            if(r){ write(fd_2, buffer, buf_len); write(fd_3, buffer, buf_len); }
            if (ret <= 0) break; // if write() gives -1(error) or a 0(EOF)

            buf_len = recv(client_fd, buffer, sizeof(buffer), 0);
        }
    }
    close_file(fd);
    if(r){ close(fd_2); close(fd_3); }
    send_response(client_fd, HTTP_CREATED);
    return;
}


// File Operations--------------------------------------------------------------

/* test_name
Tests a given string for character compliance.
Checks length and attempts to match a string via REGEX.
Matches against any combination of upper/lowercase letters or digits.
I miss Perl's regex splitting. --Rory
@params - str: the testee
@returns true or false */
int Worker::test_name(string str){
    return (str.size() == FILE_NAME_SIZE && regex_match(str.c_str(), regex(REGEX)));
}

/* close_fd
Closes and unlocks the Worker's current file, based on the given fd and Worker's
file_name field.
This should be used instead of manual closing lest we risk failing to release 
the lock.
@params - fd: the file to be closed and released
@return void */
void Worker::close_file(int fd){
    close(fd);
    shared_locker->unlock_file(file_name);
    return;
}

/* find_returnable
Runs each version of file_name (in copy[1...3]) through a series of checks to
determine which, if any, is a valid return candadite.
Does NOT close the returned fd if it finds a valid one.
Only meant to be called if redundancy is enabled!
Relies heavily on compare_files.
@params - 
@returns    fd to a returnable file,
            -1 on permission error,
            -2 on file not found,
            -3 on other error */
int Worker::find_returnable(){
    const int PERMISSION_ERROR = -1, NOT_FOUND_ERROR = -2, SERVER_ERROR = -3;
    int toReturn = 0;
    int e_permission = 0, e_missing = 0, e_other = 0;
    int fds[4]; // store the file descriptors
    // NOTE: the fd array runs from 1-3 and 0 is empty, to match the # of the copy folders.

    // create file names
    char fn[17]; // copy#/########## + 1 for terminator
    // try to open all three files
    for(int i = 1; i < 4; i++){
        snprintf(fn, sizeof(fn), "copy%d/%s", i, file_name.c_str());
        fds[i] = open(fn, O_RDONLY);
        if(fds[i] == -1){
            // permission error
            if(errno == EACCES)         e_permission += 1;
            // not found error
            else if(errno == ENOENT)    e_missing += 1;
            // uncaught error
            else                        e_other += 1;
        }
    }
    if(e_permission > 1)    toReturn = PERMISSION_ERROR;
    else if(e_missing > 1)  toReturn = NOT_FOUND_ERROR;
    else if(e_other > 1)    toReturn = SERVER_ERROR;
    else{// at least two of the fds are valid
        /* compare until two match and return one of the two.
        the third doesn't matter if two match.
        if none ever match, return server error
        this could probably be looped but who gives a fuck.
        This double and-ing only works because of C's hort circuiting;
            if one check fails, compare_files is never called and therefore
            doesn't err */
        if(     fds[1] != -1 && fds[2] != -1 && compare_files(fds[1], fds[2]))
            toReturn = fds[1];
        else if(fds[1] != -1 && fds[3] != -1 && compare_files(fds[1], fds[3]))
            toReturn = fds[1];
        else if(fds[2] != -1 && fds[3] != -1 && compare_files(fds[2], fds[3]))
            toReturn = fds[2];
        else toReturn = SERVER_ERROR; // all three are different
    }

    // close every still-open fd that isn't toReturn
    for(int i = 1; i < 4; i++) if(fds[i] != -1 && fds[i] != toReturn) close(fds[i]);
    return toReturn;
}

/* compare_files
Reads in and compares two files descriptors.
The subroutine loops through each file, half of max_buf at a time, until it runs
out of bytes to read or finds a difference
Overview of the loop:
1. Read in a section of each file
2. Return -1 on an error
3. Return 0 if the # of bytes read or the contents of each buffer differ
4. Break the loop if a_fd is out of bytes
    NOTE: we don't need to check b because either a != b and we will break
    anyways or a == b and therefore an empty a is an empty b.
5. Loop.

@params - a_fd: file desc a
        - b_fd: file desc b
@returns 1 if they are identical, 0 if not, and -1 on error */
int Worker::compare_files(int a_fd, int b_fd){

    int same = 1;
    int a_ret, b_ret; // return values of each read
    char a_buf[THREAD_BUFFER_SIZE/2], b_buf[THREAD_BUFFER_SIZE/2];

    while(same == 1){
        // read in a
        a_ret = read(a_fd, a_buf, sizeof(a_buf));
        // read in b
        b_ret = read(b_fd, b_buf, sizeof(b_buf));
        // error check
        if(a_ret == -1 || b_ret == -1){errno_kill("FILE_COMPARE", 0); same = -1;}
        else{
            if(a_ret != b_ret) same = 0; // different number of bytes were read
            else{ // compare buffers
                int ret = strncmp(a_buf, b_buf, sizeof(a_buf));
                if (ret != 0) same = 0; // unequal
            }
            if (a_ret == 0) break;
        }
    }

    return same;
}

// Response Creation------------------------------------------------------------

/* send_response
Sends the HTTP header only as response to client. Doesn't send file data.
OK, Created, Bad Request, Forbidden, Not Found, Internal Server Error
@params - int: The socket to send the response to. Usually the client.
        - char*: The message to send. ie HTTP_OK
        - ssize_t: The length of the content.
@returns void */
void Worker::send_response(int socket, const char* response, ssize_t content_len){

    if (socket <= 0) errno_kill("NO_SOCKET_RESPONSE", 1);

    char header[RESPONSE_SIZE];
    memset(header, 0, sizeof(header));

    // "HTTP\1.1 "
    strcat(header, VERSION_HTTP);
    strcat(header, " ");

    // "HTTP\1.1 200 OK\r\n"
    strcat(header, response);
    strcat(header, "\r\n");

    // "HTTP\1.1 200 OK\r\nContent-Length:0"
    strcat(header, "Content-Length: ");
    strcat(header, to_string(content_len).c_str());
    strcat(header, "\r\n\r\n");

    // send completed msg; error check
    if(send(socket, header, strlen(header), 0) == -1) errno_kill("HEADER_SEND", 0);

    return;
}

// Other------------------------------------------------------------------------
/* copy
Helper function to copy variables of Worker
@params -Worker object to be copied 
@returns void */
void Worker::copy(const Worker &source){
    thread = source.thread;
    strcpy(buffer, source.buffer);

    return;
}
