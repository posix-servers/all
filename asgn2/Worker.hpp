#ifndef WORKER_H
#define WORKER_H

// includes
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include "Lock_Manager.hpp"
#include <sys/types.h>      // socket, bind, listen, accept, connect, send, recv
#include <sys/socket.h>     // socket, bind, listen, accept, connect, send, recv
#include <fcntl.h>          // used for open()
#include <unistd.h>         // used for read(), write(), close()
#include <regex>            // used for name testing

// Server
#define DEFAULT_PORT    "80"
#define BACK_LOG        500
#define RESPONSE_SIZE   4096    // 4 KiB
#define FILE_NAME_SIZE  10 
#define DELIMITERS      " \r\n"
#define REGEX           "[a-zA-Z0-9]*"
#define VERSION_HTTP    "HTTP/1.1"

#define THREAD_BUFFER_SIZE 16384 // 16 KiB
// HTTP Codes
#define HTTP_CONTINUE              "100 Continue"
#define HTTP_OK                    "200 OK"
#define HTTP_CREATED               "201 Created"
#define HTTP_BAD_REQUEST           "400 Bad Request"
#define HTTP_FORBIDDEN             "403 Forbidden"
#define HTTP_NOT_FOUND             "404 Not Found"
#define HTTP_INTERNAL_SERVER_ERROR "500 Internal Server Error"

enum request_type {EMPTY = 0, GET = 1, PUT = 2};

/* Worker.hpp
*/
class Worker{
private:

    // Personal data
    pthread_t thread;
    char buffer[THREAD_BUFFER_SIZE];
    bool is_working;
    int id;

    // Data from Server
    Lock_Manager* shared_locker; 
    request_type request;
    int client_fd;
    std::string file_name;
    long content_length;
    ssize_t header_len;

public:
    /*--------------------------------------------------------------------------
    Constructors
    --------------------------------------------------------------------------*/
    Worker(Lock_Manager *l, int id);
    Worker(const Worker &source);
    /*--------------------------------------------------------------------------
    Destructor
    --------------------------------------------------------------------------*/
    ~Worker();

    /*--------------------------------------------------------------------------
    Operators
    --------------------------------------------------------------------------*/
    Worker& operator=(const Worker &source);

    /*--------------------------------------------------------------------------    
    Public Functions
    --------------------------------------------------------------------------*/
    pthread_t* get_thread();
    char* get_buffer();
    bool* get_is_working();

    /*--------------------------------------------------------------------------    
    Private Functions
    --------------------------------------------------------------------------*/
private:
    friend void*    start_working(void* data);
    void            execute();
    void            service_client();
    // Request Handling
    ssize_t         to_buffer(const int fd);
    void            parse_request(char* header);
    void            do_get_request();
    void            do_put_request();
    // File Operations
    void            close_file(int fd);
    int             find_returnable();
    int             test_name(std::string str);
    int             compare_files(int a_fd, int b_fd);
    void            send_response(int socket, const char* response, ssize_t content_len = 0);
    void            copy(const Worker &source);
};
#endif
