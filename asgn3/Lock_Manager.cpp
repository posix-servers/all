#include "Lock_Manager.hpp"

using std::unordered_map;
using std::deque;
using std::string;

/* Lock_Manager.cpp
Implements the class "Lock_Manager". */

/*------------------------------------------------------------------------------
Constructors
------------------------------------------------------------------------------*/
Lock_Manager::Lock_Manager(int r){
    // initalize semaphores
    sem_init(&global_lock, 0, 1);       // file global, binary
    sem_init(&q_lock, 0, 1);            // queue access, binary
    sem_init(&q_empty, 0, QUEUE_SIZE);  // # of empty slots in vector
    sem_init(&q_full, 0, 0);            // # of filled slots in vector


    // save bool redundancy
    redun = r;

    // build hashtable of files
    build_hash();
}

Lock_Manager::Lock_Manager(const Lock_Manager &source){
    copy(source);
    return;
}

/*------------------------------------------------------------------------------
Destructor
------------------------------------------------------------------------------*/
/* In the current implementation, the dest is unlikely to ever be called as the
prog will be force-killed */
Lock_Manager::~Lock_Manager(){
    // deallocate all strings and sem_t* in hash
    for(auto it = file_locks.begin(); it != file_locks.end(); ++it){ 
        sem_destroy(it->second); 
        delete it->second;
        //it->first.clear();
    }
}

/*------------------------------------------------------------------------------
Operators
------------------------------------------------------------------------------*/
// Assignment operator
Lock_Manager& Lock_Manager::operator=(const Lock_Manager &source){
    copy(source);

    return *this;
}


/*------------------------------------------------------------------------------
Public Functions
------------------------------------------------------------------------------*/
// File operations--------------------------------------------------------------
/* lock_file
Queries the hashtable for the lock and acquires it.
Used in tandem with unlock_file.
@params - filename: what it says on the tin
@returns 1 on success, 0 if file does not exist */
int Lock_Manager::lock_file(string filename){
    int toReturn = 1;

    // query hash
    auto l = file_locks.find(filename);
    if(l != file_locks.end()) sem_wait(l->second);
    else toReturn = 0;

    return toReturn;
}
/* unlock_file
Queries the hashtable for the lock and releases it.
BECAUSE THE FILE LOCKS ARE BINARY SEMAPHORE: unlock_file() tests the value first
using sem_getvalue().
Used in tandem with lock_file.
@params - filename: what it says on the tin
@returns 1 on success, 0 if file does not exist */
int Lock_Manager::unlock_file(string filename){
    int toReturn = 1;

    // query hash
    auto l = file_locks.find(filename);
    if(l != file_locks.end()){
        int val;
        // test value
        sem_getvalue(l->second, &val);
        if(val > 0) fprintf(stderr, "FILE_LOCK_ATTEMPT_NON_BINARY\n");
        else sem_post(l->second);
    }
    else toReturn = 0;

    return toReturn;
}
/* add_lock
Adds a new entry, with initalized binary semaphore, to hash.
Locks the hash in the process to prevent mutliple writes.
@params - filename: what it says on the tin
@returns 1 on success, CURRENTLY HAS NO FAILURE CONDITION */
int Lock_Manager::add_lock(string filename){

    int toReturn = 1;

    sem_wait(&global_lock); // lock hash
    // only add the lock if the file does not already exist
    if(file_locks.find(filename) == file_locks.end()){
        sem_t* new_s = new sem_t;
        sem_init(new_s, 0, 1); //initalize the struct as a binary semaphore
        file_locks.insert({filename, new_s});
    }

    sem_post(&global_lock); // unlock hash

    return toReturn;
}

/* build_hash
Populates the hashtable of file_locks by reading through every file in the
current directory, adding them to the hash, and then initalizing their binary
semaphore.
@params - 
@returns 1 on success, 0 on failure. */
int Lock_Manager::build_hash(){
    int status = 1;
    file_locks.clear();
    if(!redun) parse_dir(".");
    else{
        parse_dir("copy1");
        parse_dir("copy2");
        parse_dir("copy3");
    }

    return status;
}

// Client Queue operations------------------------------------------------------
/* queue_client
Called by the dispatcher to add a client to the queue.
@params - fd: the file descriptor of the client
@returns 1 on success, CURRENTLY HAS NO FAILURE CONDITION */
int Lock_Manager::queue_client(int fd){
    int toReturn = 1;

    sem_wait(&q_empty); // wait for empty spaces
    sem_wait(&q_lock);  // lock the buffer
    clients.push_back(fd);
    sem_post(&q_lock);  // release the buffer
    sem_post(&q_full);  // indicated new filled space

    return toReturn;
}
/* dequeue_client
Called by a worker to pull a client from the queue.
@params - 
@returns pointer to the dequeued fd or NULL on error (NYI) */
int Lock_Manager::dequeue_client(){
    int toReturn = -1;

    sem_wait(&q_full); // wait for a filled space
    sem_wait(&q_lock);  // lock the buffer
    // retrieve the item
    toReturn = clients.front();
    clients.pop_front();
    sem_post(&q_lock);  // release the buffer
    sem_post(&q_empty);  // indicated new filled space


    return toReturn;
}

// Misc Information Storage-----------------------------------------------------
/* redundnacy
@returns the redunancy field*/
int Lock_Manager::redundancy(){ return redun; }
/*------------------------------------------------------------------------------
Private Functions
------------------------------------------------------------------------------*/
/* parse_dir 
Helper function for build_hash.
Parses the directory named by the parameter and adds unknown files to the hash 
@params - d: the name of the directory to be parsed
@returns 1 on success, 0 on failure */
int Lock_Manager::parse_dir(const char* d){
    int toReturn = 1;
    DIR* dirp = opendir(d); // open current directory
    struct dirent* dp;

    // loop through d and create a lock for each file
    while ((dp = readdir(dirp)) != NULL) {
        string fn_str(dp->d_name);
        
        // check if we have already added this file
        if( file_locks.find(dp->d_name) == file_locks.end() && // not in table
            dp->d_type == DT_REG &&         // is a 'regular' file
            fn_str != "b" &&                // not BACKUP
            fn_str != "l" &&                // not LIST
            fn_str != "r"){                 // not RESTORE
            // unknown file! add it!
            sem_t* t_lock = new sem_t; // declare the struct
            sem_init(t_lock, 0, 1); // initalize the struct as a binary semaphore
            file_locks.insert({fn_str, t_lock});
        }
    }

    if(errno != 0){
        errno_kill("BUILD_HASH", 0);
        toReturn = 0;
    }

    closedir(dirp);
    return toReturn;
}


/* copy
Helper function to copy variables of Lock_Manager
WARNING: copies references of file_locks, not values
@params -Lock_Manager object to be copied 
@returns void */
void Lock_Manager::copy(const Lock_Manager &source){

    global_lock = source.global_lock;
    redun = source.redun;
    file_locks = source.file_locks; // WARNING copies references, not values

    return;
}
