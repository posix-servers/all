#ifndef LOCK_MANAGER_H
#define LOCK_MANAGER_H

// includes
//#include <vector>
#include <semaphore.h>
#include <unordered_map>    // C++ Hashtable
#include <dirent.h>         // DIR* and directory parsing
#include <stdio.h>
#include <string.h>
#include <string>
#include "utils.hpp"        // contains errno_kill
#include <deque>            // client_fd queue

#define QUEUE_SIZE 500

/* Lock_Manager.hpp
Defines the class "Lock_Manager". */
class Lock_Manager{
private:
    sem_t               global_lock;
    std::unordered_map<std::string, sem_t*>   file_locks; // filename -> file mutex
    int                 redun;
    // client queue variables
    std::deque<int>     clients;
    sem_t               q_lock; // binary, locks the vector
    sem_t               q_empty; // tracks # of empty slots in vector
    sem_t               q_full; // tracks # of filled slots in vector
public:
    /*--------------------------------------------------------------------------
    Constructors
    --------------------------------------------------------------------------*/
    Lock_Manager(int r);
    Lock_Manager(const Lock_Manager &source);
    /*--------------------------------------------------------------------------
    Destructor
    --------------------------------------------------------------------------*/
    ~Lock_Manager();

    /*--------------------------------------------------------------------------
    Operators
    --------------------------------------------------------------------------*/
    Lock_Manager& operator=(const Lock_Manager &source);

    /*--------------------------------------------------------------------------    
    Public Functions
    --------------------------------------------------------------------------*/
    // File operations
    int         lock_file(std::string filename);
    int         unlock_file(std::string filename);
    int         add_lock(std::string filename);
    int         build_hash();
    // Client Queue operations
    int         queue_client(int fd);
    int         dequeue_client();
    // Misc Information Storage
    int         redundancy();
    // Special Request Operations
    int         regenerate_hash();
    /*--------------------------------------------------------------------------    
    Private Functions
    --------------------------------------------------------------------------*/
private:
    int         parse_dir(const char* d);
    void        copy(const Lock_Manager &source);
    
};
#endif
