#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

/* utils.hpp 
Any and all protected, static subroutines shared by the other files. */

void errno_kill(const char* id, bool kill);

#endif